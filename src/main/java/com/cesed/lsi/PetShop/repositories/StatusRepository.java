package com.cesed.lsi.PetShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cesed.lsi.PetShop.model.Status;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

}
