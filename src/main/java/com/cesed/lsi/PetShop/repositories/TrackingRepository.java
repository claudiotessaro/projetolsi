package com.cesed.lsi.PetShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cesed.lsi.PetShop.model.Tracking;

@Repository
public interface TrackingRepository extends JpaRepository<Tracking, Long>{

}
