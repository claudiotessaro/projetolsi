package com.cesed.lsi.PetShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cesed.lsi.PetShop.model.Entrega;

/***
 * Essa classe é responsavel por criar o repositorio de entrega, e através dessa anotação @RepositoryRestResource, ele também ja cria os services e ja cria os ednpoints da entrega
 * @author tessa
 *
 */
@RepositoryRestResource(collectionResourceRel = "entrega", path="entregas")
public interface EntregaRepository extends JpaRepository<Entrega, Long> {

}
