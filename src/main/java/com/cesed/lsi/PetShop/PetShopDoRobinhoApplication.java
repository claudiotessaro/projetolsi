package com.cesed.lsi.PetShop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cesed.lsi.PetShop.repositories.EntregaRepository;
import com.cesed.lsi.PetShop.repositories.TipoPagamentoRepository;
import com.cesed.lsi.PetShop.service.StatusService;
import com.cesed.lsi.PetShop.service.TrackingService;

@SpringBootApplication
public class PetShopDoRobinhoApplication implements CommandLineRunner {
	@Autowired
	StatusService statusService;

	@Autowired
	TrackingService trackingService;

	@Autowired
	EntregaRepository entregaRepository;

	@Autowired
	TipoPagamentoRepository tPagamento;

	public static void main(String[] args) {
		SpringApplication.run(PetShopDoRobinhoApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {

	}
}
