package com.cesed.lsi.PetShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cesed.lsi.PetShop.model.Status;
import com.cesed.lsi.PetShop.repositories.StatusRepository;

@Service
public class StatusService {

	@Autowired
	private StatusRepository statusRepository;
	

	public Status inserirStatus(Status status) {
		return statusRepository.save(status);
	}

	public Status getById(Long id) {
		Status status = statusRepository.findById(id).get();
		return status;
	}

}
