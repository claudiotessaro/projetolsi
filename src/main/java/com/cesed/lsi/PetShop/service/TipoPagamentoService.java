package com.cesed.lsi.PetShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cesed.lsi.PetShop.model.TipoPagamento;
import com.cesed.lsi.PetShop.repositories.TipoPagamentoRepository;


/***
 * Service para inserção dos tipos de pagamento
 * @author tessa
 *
 */
@Service
public class TipoPagamentoService {
	
	@Autowired
	private TipoPagamentoRepository tipoRepository;
	
	/***
	 * Metodo que pega um pagamento pelo id;
	 * @param id
	 * @return
	 */
	public TipoPagamento getById(Long id) {
		return tipoRepository.findById(id).get();
	}
	
	/***
	 * Metodo que insere um tipo de pagamento
	 * @param tipoPagamento
	 */
	public void inserirTipo(TipoPagamento tipoPagamento) {
		tipoRepository.save(tipoPagamento);
	}

}
