package com.cesed.lsi.PetShop.service.mapper;


import com.cesed.lsi.PetShop.dto.pedido.CadastrarPedidoDTO;
import com.cesed.lsi.PetShop.model.Pedido;
import com.cesed.lsi.PetShop.model.TipoPagamento;


/***
 * Classe responsavel pro fazer a conversão do objeto DTO para o objeto
 * @author claudio aragao tessaro
 *
 */
public class PedidoMapper {
	
	public static Pedido mapper(CadastrarPedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido();
		pedido.setDataStatus(pedidoDTO.getDataStatus());
		pedido.setValorTotal(pedidoDTO.getValorTotal());	
		return pedido;
	}

}
