package com.cesed.lsi.PetShop.service.mapper;

import com.cesed.lsi.PetShop.dto.pedido.CadastrarTrackingDTO;
import com.cesed.lsi.PetShop.model.Tracking;

/***
 * Classe responsavel pro fazer a conversão do objeto DTO para o objeto
 * @author claudio tessaro
 *
 */
public class TrackingMapper {
	
	public static Tracking mapper(CadastrarTrackingDTO trackingDTO) {
		Tracking tracking = new Tracking();
		tracking.setDataStatus(trackingDTO.getDataStatus());
		tracking.setDescricao(trackingDTO.getDescricao());
		tracking.setGeo_long(trackingDTO.getGeo_long());
		tracking.setGeoLat(trackingDTO.getGeoLat());
		return tracking;
	}

}
