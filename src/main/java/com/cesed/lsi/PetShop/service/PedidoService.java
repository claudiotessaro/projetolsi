package com.cesed.lsi.PetShop.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cesed.lsi.PetShop.model.Pedido;
import com.cesed.lsi.PetShop.model.TipoPagamentoEnum;
import com.cesed.lsi.PetShop.repositories.PedidoRepository;


/***
 * Serviço responsavel por implementar as regras de negocios no meu pedido
 * @author claudio aragão tessaro
 *
 */
@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private TipoPagamentoService tipoService;

	
	/***
	 * Metodo que insere os meus pedidos, passando na requisição um objeto json com os dados de pedido e os dados do tipo de pagamento. 
	 * Foi feito uma regra de negocio com relação a um desconto caso a escolha de pagamento seja avista ou no cartão
	 * @param pedido
	 * @return
	 */
	public Pedido inserirPedido(Pedido pedido) {
		double desconto;
		double valorPedido;
		tipoService.inserirTipo(pedido.getTipoPagamento());
		pedido.setTipoPagamento(tipoService.getById(pedido.getTipoPagamento().getId()));
		valorPedido = pedido.getValorTotal();
		if (pedido.getTipoPagamento().getNomeTipoPgto().equals(TipoPagamentoEnum.AVISTA)) {
			desconto = 0.15;
				
		}else {
			desconto = 0.10;
		}
		valorPedido -= valorPedido*desconto;
		pedido.setValorTotal(valorPedido);
		return pedidoRepository.save(pedido);
	}
	
	/***
	 * Metodo atualiza um pedido quando é passado um id;
	 * @param pedido
	 * @param id
	 * @return
	 */

	public Pedido update(Pedido pedido, Long id) {
		Pedido ped = getById(id);
		tipoService.inserirTipo(pedido.getTipoPagamento());
		ped.setDataStatus(pedido.getDataStatus());
		ped.setTipoPagamento(tipoService.getById(pedido.getTipoPagamento().getId()));
		return pedidoRepository.save(ped);

	}
	
	/**
	 * Metodo que deleta um pedido pelo id
	 * @param id
	 */

	public void delete(Long id) {
		pedidoRepository.deleteById(id);
	}
	
	/***
	 * Metodo que retorna um pedido passando um id
	 * @param id
	 * @return
	 */

	public Pedido getById(Long id) {
		return pedidoRepository.findById(id).get();
	}

	/***
	 * Metodo que retorna todos os pedidos ja inseridos
	 * @return
	 */
	public List<Pedido> findAll() {
		List<Pedido> pedidos = pedidoRepository.findAll();
		return pedidos;
	}

}
