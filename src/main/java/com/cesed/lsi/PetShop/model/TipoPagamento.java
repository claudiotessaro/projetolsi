package com.cesed.lsi.PetShop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/***
 * Entidade que modela o tipo de pagamento
 * @author claudio tessaro
 *
 */
@Entity
@Table(name= "tipo_pgto")
public class TipoPagamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="nome_tipo_pagto")
	private TipoPagamentoEnum nomeTipoPgto;
	
	@Enumerated(EnumType.STRING)
	private AtivoEnum ativo;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public TipoPagamentoEnum getNomeTipoPgto() {
		return nomeTipoPgto;
	}

	public void setNomeTipoPgto(TipoPagamentoEnum nomeTipoPgto) {
		this.nomeTipoPgto = nomeTipoPgto;
	}

	public AtivoEnum getAtivo() {
		return ativo;
	}

	public void setAtivo(AtivoEnum ativo) {
		this.ativo = ativo;
	}

}
