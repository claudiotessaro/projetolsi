package com.cesed.lsi.PetShop.model;

/***
 * Enum responsavel por afirmar se o pagamento ta em aberto ou ja esta fechado
 * @author tessa
 *
 */
public enum AtivoEnum {
	
	S("Sim"),
	N("Nao");
	
	private String descricao;
	
	private AtivoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
