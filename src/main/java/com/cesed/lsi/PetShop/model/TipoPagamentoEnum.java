package com.cesed.lsi.PetShop.model;

/***
 * Enum que mostra qual o tipo de pagamento selecionado pelo cliente
 * @author tessa
 *
 */
public enum TipoPagamentoEnum {
	
	CARTAO("Cartao"),
	AVISTA("A vista");
	
	private String descricao;
	
	private TipoPagamentoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	

}
